package agh.edu.javaoop2;

import com.sun.org.apache.xpath.internal.operations.Bool;
import junit.framework.TestCase;

import java.util.Random;

/**
 * Created by kveld on 4/9/16.
 */
public class ManagerTest extends TestCase {

    public void testNullContract() throws Exception{
        Double budgetLimit = 20000.0;
        Integer employeeCnt = 5;

        Manager managerWithBudgetLimit = new Manager("Mordechai", budgetLimit);
        assertNull(managerWithBudgetLimit.getEmployeeCountLimit());
        Manager managerWithCountLimit = new Manager("Alojzy", employeeCnt);
        assertNull(managerWithCountLimit.getBudgetLimit());
    }

    public void testCountLimit() {
        Integer employeeCnt = new Random().nextInt() % 16;
        Manager managerWithCountLimit = new Manager("Wladyslaw", employeeCnt);
        for(int i = 0; i < 2 * employeeCnt;  i++) {
            Boolean isSuccess = managerWithCountLimit.hireEmployee(new Employee("test", 0.0));
            if(i + 1 <= employeeCnt) {
                assertTrue(isSuccess);
            }
            else {
                assertFalse(isSuccess);
            }
            assertTrue(managerWithCountLimit.getSubordinates().size() <= employeeCnt);
        }
    }

    public void testBudgetLimit() {
        Double budgetLimit = new Random().nextDouble() * 100000.0;
        Manager managerWithBudgetLimit = new Manager("Stanislaw", budgetLimit);
        for(int i = 0; i < 100; i++) {
            Double salary = new Random().nextDouble() * 20000.0;
            Boolean isSuccess;
            if(managerWithBudgetLimit.getSubordinatesSalarySum() + salary <= managerWithBudgetLimit.getBudgetLimit()) {
                isSuccess = managerWithBudgetLimit.hireEmployee(new Employee("test", salary));
                assertTrue(isSuccess);
            }
            else {
                isSuccess = managerWithBudgetLimit.hireEmployee(new Employee("test", salary));
                assertFalse(isSuccess);
            }
            assertTrue(managerWithBudgetLimit.getSubordinatesSalarySum() <= managerWithBudgetLimit.getBudgetLimit());
        }
    }
}
