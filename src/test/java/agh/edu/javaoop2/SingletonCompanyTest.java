package agh.edu.javaoop2;

import junit.framework.TestCase;

import java.util.Random;
import java.util.Scanner;

/**
 * Created by kveld on 4/9/16.
 */
public class SingletonCompanyTest extends TestCase{
    public void testToString() throws Exception {
        SingletonCompany testCompany = SingletonCompany.getInstance();
        CEO testBoss = new CEO("Tomasz");
        testCompany.hireCEO(testBoss);
        for(int i = 0; i < 10; i++) {
            Boolean employeeType = new Random().nextBoolean();
            if(employeeType) {
                testCompany.getBoss().hireEmployee(new Manager("testManager", 5000));
            }
            else {
                testCompany.getBoss().hireEmployee(new Employee("testEmployee", 10.0));
            }
        }
        for(Employee e : testCompany.getBoss().getEmployees()) {
            if(e instanceof Manager) {
                Integer hireCnt = new Random().nextInt() % 5;
                for(int i = 0; i < hireCnt; i++) {
                    ((Manager) e).hireEmployee(new Employee("testSubordinate", 10.0));
                }
            }
        }
        String testString = testCompany.toString();


        Scanner sc = new Scanner(testString);
        sc.useDelimiter("\n");
        assertTrue(sc.nextLine().contains("CEO"));

        int i = 0;
        while(sc.hasNextLine()) {
            String currentLine = sc.nextLine();
            assertTrue(currentLine.contains("\t"));
            if(testCompany.getBoss().getEmployees().get(i) instanceof Manager) {
                for(Employee e : ((Manager) testCompany.getBoss().getEmployees().get(i)).getSubordinates()) {
                    String subLine = sc.nextLine();
                    assertTrue(subLine.contains("\t\t"));
                }
            }
            i += 1;
        }
    }
}
