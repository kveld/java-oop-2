package agh.edu.javaoop2;

import junit.framework.TestCase;

import java.util.Random;

/**
 * Created by kveld on 4/9/16.
 */
public class EmployeeTest extends TestCase{

    public void testAnswers() throws Exception{
        String name = "Janusz";
        Employee testEmployee = new Employee(name, 0.0);
        for(int i = 0; i < 10; i++) {
            Double randomSalary = new Random().nextDouble() * 20000.0;
            testEmployee.setSalary(randomSalary);
            assertEquals(name, testEmployee.getName());
            assertEquals(Double.toString(randomSalary), testEmployee.getSalaryAsAnswer());
            if(randomSalary > Employee.SATISFACTION_THRESHOLD) {
                assertEquals(testEmployee.getSatisfiedAsAnswer(), "Yes");
            }
            else {
                assertEquals(testEmployee.getSatisfiedAsAnswer(), "No");
            }
        }
    }
}
