package agh.edu.javaoop2;

import java.util.ArrayList;

/**
 * Created by kveld on 4/6/16.
 */
public class Manager extends Employee {
    private final Double budgetLimit;
    private final Integer employeeCountLimit;
    private ArrayList<Employee> subordinates;
    private Double subordinatesSalarySum;

    /*
     * @param budgetLimit - maximum sum of subordinate employees salaries
     *
     * Creating a Manager object with a budgetLimits implies it has no
     * employeeCountLimit
     */
    public Manager(String name, Double budgetLimit) {
        this.name = name;
        this.budgetLimit = budgetLimit;
        this.subordinatesSalarySum = 0.0;
        this.employeeCountLimit = null;
        setSubordinates(new ArrayList<Employee>());
    }

    /*
     * @param employeeCountLimit - maximum size of the group of Manager's subordinates
     *
     * Creating a Manager object with an employeeCountLimit implies it has no
     * budgetLimit
     */
    public Manager(String name, Integer employeeCountLimit) {
        this.name = name;
        this.employeeCountLimit = employeeCountLimit;
        this.budgetLimit = null;
        setSubordinates(new ArrayList<Employee>());
    }

    /*
     * @param employee - an Employee object to add to Manager's subordinates
     *
     * @return True if an employee was successfully added to the Manager's subordinates.
     *         False otherwise
     */
    public Boolean hireEmployee(Employee employee) {
        if(getBudgetLimit() != null) {
            if(getSubordinatesSalarySum() + employee.getSalary() <= getBudgetLimit() &&
                    getSubordinatesSalarySum() < Double.MAX_VALUE - employee.getSalary()) {
                getSubordinates().add(employee);
                setSubordinatesSalarySum(getSubordinatesSalarySum() + employee.getSalary());
                return Boolean.TRUE;
            }
            else {
                return Boolean.FALSE;
            }
        }
        else {
            if(getSubordinates().size() < getEmployeeCountLimit()) {
                getSubordinates().add(employee);
                return Boolean.TRUE;
            }
            else {
                return Boolean.FALSE;
            }
        }
    }

    public Double getSubordinatesSalarySum() {
        return subordinatesSalarySum;
    }

    public Double getBudgetLimit() {
        return budgetLimit;
    }

    public Integer getEmployeeCountLimit() {
        return employeeCountLimit;
    }

    public ArrayList<Employee> getSubordinates() {
        return subordinates;
    }

    public void setSubordinates(ArrayList<Employee> subordinates) {
        this.subordinates = subordinates;
    }

    private void setSubordinatesSalarySum(Double subordinatesSalarySum) {
        this.subordinatesSalarySum = subordinatesSalarySum;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(name + " - Manager\n");
        for (Employee subordinate : subordinates) {
            sb.append("\t\t" + subordinate.toString());
        }
        return sb.toString();
    }
}
