package agh.edu.javaoop2;

import java.util.ArrayList;

/**
 * Created by kveld on 4/7/16.
 */
public class CEO {
    private String name;
    private ArrayList<Employee> employees;

    public CEO(String name) {
        employees = new ArrayList<Employee>();
        this.name = name;
    }

    public void hireEmployee(Employee employee) {
        employees.add(employee);
    }

    ArrayList<Employee> getEmployees() { return employees; }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(name + " - CEO\n");
        for (Employee employee : employees) {
            sb.append("\t" + employee.toString());
        }
        return sb.toString();
    }
}
