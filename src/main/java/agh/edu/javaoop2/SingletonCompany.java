package agh.edu.javaoop2;

/**
 * Created by kveld on 4/6/16.
 */
public final class SingletonCompany {
    private static SingletonCompany instance = new SingletonCompany();
    private CEO boss;

    public static SingletonCompany getInstance() {
        return instance;
    }
    private SingletonCompany() {}

    public void hireCEO(CEO boss) {
        getInstance().boss = boss;
    }

    @Override
    public String toString() {
        return getInstance().getBoss().toString();
    }

    public CEO getBoss() {
        return getInstance().boss;
    }
}
