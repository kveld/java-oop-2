package agh.edu.javaoop2;

/**
 * Created by kveld on 4/6/16.
 */
public class Employee {
    protected String name;
    private Double salary;
    private Boolean isSatisfied;

    public static final Double SATISFACTION_THRESHOLD = 10000.0;

    public Employee() {
        this.name = null;
        this.salary = null;
        this.isSatisfied = null;
    }

    public Employee(String name, Double salary) {
        this.name = name;
        this.salary = salary;
        setSatisfaction();
    }

    public void setSalary(Double salary) {
        this.salary = salary;
        setSatisfaction();
    }

    private void setSatisfaction() {
        isSatisfied = salary > Employee.SATISFACTION_THRESHOLD;
    }

    Double getSalary() {return salary;}

    public String getSalaryAsAnswer() {
        return Double.toString(salary);
    }

    public String getName() {
        return name;
    }

    public String getSatisfiedAsAnswer() {
        return isSatisfied ? "Yes" : "No";
    }

    @Override
    public String toString() {
        return name + " - Employee\n";
    }
}
