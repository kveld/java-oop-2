package agh.edu.javaoop2;

/**
 * Created by kveld on 4/8/16.
 */
public class CompanyDemo {
    public static void main(String[] args) {
        SingletonCompany myCompany = SingletonCompany.getInstance();
        CEO Janusz = new CEO("Janusz");
        Manager Krzysztof = new Manager("Krzysztof", 50000.0);
        Employee Jeremiasz = new Employee("Jeremiasz", 10000.0);
        Employee Marek = new Employee("Marek", 11000.0);
        Employee Marzena = new Employee("Marzena", 3000.0);
        Employee Konrad = new Employee("Konrad", 30000.0);
        Manager Maciej = new Manager("Maciej", 3);
        Krzysztof.hireEmployee(Konrad);
        Maciej.hireEmployee(Marek);
        Maciej.hireEmployee(Marzena);
        Janusz.hireEmployee(Jeremiasz);
        Janusz.hireEmployee(Maciej);
        Janusz.hireEmployee(Krzysztof);
        myCompany.hireCEO(Janusz);
        System.out.print(myCompany);
    }
}
